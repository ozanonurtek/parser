import xmltodict
from bs4 import BeautifulSoup
import requests
import pandas as pd  # now i'm data scientist ;)


def xml_to_dict():
    response = requests.get('https://www.jumbo.com.tr/category-product0.xml')
    resp_dict = xmltodict.parse(response.content)
    results = []
    index = 0
    for item in resp_dict['urlset']['url']:
        index = index + 1
        url = item['loc']
        soup = BeautifulSoup(requests.get(url).content, 'html.parser')
        name = soup.find('h2', itemprop='name').string
        price = soup.find('p', itemprop='price').string
        image_url = soup.find('link', itemprop='image')['href']
        description_tab = soup.find(id='description_tab')
        description = ''
        for s in description_tab.find_all('p'):
            if s.string:
                description = description + ' ' + s.string
        for ul in description_tab.find_all('ul'):
            for hidden_desc in ul.strings:
                if hidden_desc.string:
                    description = description + ' ' + hidden_desc.string
        result = {'name': name, 'price': price, 'image_url': image_url, 'description': description}
        results.append(result)
        print('*******', 'ITEM: ', index, ' completed', '*******')
    data_frame = pd.DataFrame(results)
    data_frame.to_excel('./export.xlsx')

if __name__ == '__main__':
    xml_to_dict()
